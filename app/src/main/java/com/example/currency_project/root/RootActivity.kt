package com.example.currency_project.root

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.currency_project.R
import com.example.currency_project.current_exchange_rate.ui.CurrentExchangeRateFragment
import com.example.currency_project.databinding.ActivityRootBinding
import com.example.currency_project.login.ui.LoginFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RootActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRootBinding
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRootBinding.inflate(layoutInflater)
        setContentView(R.layout.activity_root)

        auth = Firebase.auth
        val currentUser = auth.currentUser

        if (currentUser != null) {
            replace(CurrentExchangeRateFragment())
        } else {
            replace(LoginFragment())
        }
    }

    private fun replace(
        fragment: Fragment,
        tag: String = fragment::class.java.name
    ) {
        val fragmentManager = this.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction
            .replace(R.id.container, fragment, tag)
            .addToBackStack(tag)
            .commit()
    }
}