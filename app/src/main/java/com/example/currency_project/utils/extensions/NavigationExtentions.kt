package com.example.currency_project.utils.extensions

import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Lifecycle
import com.example.currency_project.R

private const val PREVIOUS_FRAGMENT_TAG_ARG = "PREVIOUS_FRAGMENT_TAG_ARG"
private var backPressedTime = 0L
private const val minBackPressedTime = 2000
private const val minBackStackEntryCount = 2

fun Fragment.replace(fragment: Fragment) {
    val fragmentManager = requireActivity().supportFragmentManager
    val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
    fragmentTransaction.addToBackStack(null)
        .replace(R.id.container, fragment)
        .commit()
}

fun Fragment.popScreen() {
    requireActivity().hideKeyboard()

    val fragmentManager = parentFragment?.childFragmentManager ?: childFragmentManager
    if (fragmentManager.backStackEntryCount < minBackStackEntryCount) {
        requireActivity().popFeature()
    } else {
        whenStateAtLeast(Lifecycle.State.STARTED) { fragmentManager.popBackStack() }
//        whenStateAtLeast(Lifecycle.State.STARTED) {
//            val previousFragment = fragmentManager.fragments[fragmentManager.fragments.size - 2]
//            fragmentManager.beginTransaction().hide(this).show(previousFragment).commit()
//        } нерабочая фигня
    }
}

fun FragmentActivity.popFeature() {
    if (supportFragmentManager.backStackEntryCount < minBackStackEntryCount) {
        val backText = resources.getString(R.string.back_text)
        val currentTime = System.currentTimeMillis()
        if (currentTime - backPressedTime < minBackPressedTime) {
            finish()
        } else {
            backPressedTime = currentTime
            Toast.makeText(this, backText, Toast.LENGTH_SHORT).show()
        }
    } else {
        whenStateAtLeast(Lifecycle.State.STARTED) {
            supportFragmentManager.popBackStack()
        }
    }
}
