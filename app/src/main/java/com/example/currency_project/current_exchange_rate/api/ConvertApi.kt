package com.example.currency_project.current_exchange_rate.api

import com.example.currency_project.current_exchange_rate.api.models.CurrencyDescriptionResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ConvertApi {
    @GET("v1/currencies")
    suspend fun getCurrencyDescription(
        @Query("apikey") apiKey: String,
        @Query("currencies") currencies: List<String>
    ): CurrencyDescriptionResponse

}