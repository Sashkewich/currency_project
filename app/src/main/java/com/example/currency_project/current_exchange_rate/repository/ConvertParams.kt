package com.example.currency_project.current_exchange_rate.repository

data class ConvertParams(
    val apiKey: String,
    val currencies: List<String>
)
