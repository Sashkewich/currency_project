package com.example.currency_project.current_exchange_rate.di

import com.example.currency_project.current_exchange_rate.api.CurrencyApi
import com.example.currency_project.current_exchange_rate.interactor.CurrentExchangeRateInteractor
import com.example.currency_project.current_exchange_rate.repository.CurrentExchangeRateRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object CurrentExchangeRateHiltModule {
    @Provides
    @Singleton
    fun provideApi(retrofit: Retrofit): CurrencyApi = retrofit.create(CurrencyApi::class.java)

    @Provides
    @Singleton
    fun provideInteractor(repository: CurrentExchangeRateRepository): CurrentExchangeRateInteractor =
        CurrentExchangeRateInteractor(repository)
}