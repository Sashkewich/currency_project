package com.example.currency_project.current_exchange_rate.di

import com.example.currency_project.current_exchange_rate.repository.CurrentExchangeRateRemoteRepository
import com.example.currency_project.current_exchange_rate.repository.CurrentExchangeRateRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindRepository(
        repository: CurrentExchangeRateRemoteRepository
    ): CurrentExchangeRateRepository
}

