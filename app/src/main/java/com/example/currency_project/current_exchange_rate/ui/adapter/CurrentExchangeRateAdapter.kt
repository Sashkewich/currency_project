package com.example.currency_project.current_exchange_rate.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.currency_project.R
import com.example.currency_project.current_exchange_rate.model.Valute

class CurrentExchangeRateAdapter (
    private val clickOnItem:(Valute) -> Unit
    ) : RecyclerView.Adapter<CurrentExchangeRateViewHolder>() {

        private val data = mutableListOf<Valute>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrentExchangeRateViewHolder {
            LayoutInflater.from(parent.context).inflate(R.layout.item_currency, parent, false)
            return CurrentExchangeRateViewHolder(parent, clickOnItem)
        }

        override fun getItemCount() = data.size

        override fun onBindViewHolder(holder: CurrentExchangeRateViewHolder, position: Int) {
            val listItem = data[position]
            holder.getData(listItem)
            holder.onBind()
        }

        @SuppressLint("NotifyDataSetChanged")
        fun setData(items: List<Valute>) {
            data.clear()
            data.addAll(items)
            notifyDataSetChanged()
        }
    }
