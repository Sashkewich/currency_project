package com.example.currency_project.current_exchange_rate.repository

import com.example.currency_project.current_exchange_rate.model.CurrencyDescription

interface ConvertRepository {
    suspend fun getCurrencyDescription(params: ConvertParams): CurrencyDescription
}