package com.example.currency_project.current_exchange_rate.interactor

import com.example.currency_project.current_exchange_rate.repository.CurrentExchangeRateRepository
import javax.inject.Inject

class CurrentExchangeRateInteractor @Inject constructor(
    private val repository: CurrentExchangeRateRepository
) {
    suspend fun getCurrentExchangeRate(
        apiKey: String,
        baseCurrency: String,
        currencies: List<String>
    ) = repository.getCurrentExchangeRate(apiKey, baseCurrency, currencies)
}