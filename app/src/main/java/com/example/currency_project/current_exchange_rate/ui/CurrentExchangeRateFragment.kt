package com.example.currency_project.current_exchange_rate.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.currency_project.R
import com.example.currency_project.common.mvvm.BaseFragment
import com.example.currency_project.current_exchange_rate.model.Valute
import com.example.currency_project.current_exchange_rate.repository.ConvertParams
import com.example.currency_project.current_exchange_rate.ui.adapter.CurrentExchangeRateAdapter
import com.example.currency_project.databinding.FragmentCurrentExchangeRateBinding
import com.example.currency_project.login.ui.LoginFragment
import com.example.currency_project.utils.extensions.removeNullsWithDot
import com.example.currency_project.utils.extensions.replace
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.util.*


@AndroidEntryPoint
class CurrentExchangeRateFragment : BaseFragment(R.layout.fragment_current_exchange_rate) {
    private lateinit var binding: FragmentCurrentExchangeRateBinding
    private lateinit var adapterItemsRecyclerView: ArrayAdapter<String>
    private lateinit var adapterItems: ArrayAdapter<String>
    private val viewModel: CurrentExchangeRateViewModel by viewModels()
    private val convertViewModel: ConvertViewModel by viewModels()
    private var currencyFrom = ""
    private var currencyTo = ""
    private var amount = 0.0
    private var convertResult = 0.0

    override fun onResume() {
        super.onResume()
        val currencies = resources.getStringArray(R.array.valutes_array)
        adapterItems =
            ArrayAdapter(this.requireContext(), R.layout.valutes_spinner_list, currencies)

        with(binding) {
            autoCompleteTextFirstViewCurrency.setAdapter(adapterItems)
            autoCompleteTextViewSecondCurrency.setAdapter(adapterItems)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCurrentExchangeRateBinding.inflate(inflater)
        return binding.root
    }

    private val adapter: CurrentExchangeRateAdapter by lazy {
        CurrentExchangeRateAdapter { item ->
            println(item)
        }
    }

    private val layoutManager: LinearLayoutManager by lazy {
        LinearLayoutManager(context)
    }

    @SuppressLint("SetTextI18n", "ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val apiKey = resources.getString(R.string.api_key)
        val baseCurrency = CurrencyType.USD.currency
        val currencies = emptyList<String>()
        observe(viewModel.loadingStateFlow) {
            showLoading(true)
        }

        try {
            viewModel.getCurrentExchangeRate(apiKey, baseCurrency, currencies)
        } catch (ex: Exception) {
            Toast.makeText(context, "Sorry, but data don't downloaded", Toast.LENGTH_SHORT).show()
        } finally {
            observe(viewModel.loadingStateFlow) {
                showLoading(false)
            }
        }

        observe(viewModel.currencyData) { currencyData ->
            Timber.i("/*/ observe works")
            showData(currencyData.currencies)
        }

        with(binding) {
            recyclerViewCurrencyList.layoutManager = layoutManager
            recyclerViewCurrencyList.setHasFixedSize(true)
            recyclerViewCurrencyList.adapter = adapter
            adapter.onAttachedToRecyclerView(recyclerViewCurrencyList)

        }

        val listOfCurrencies = listOf(
            "EUR",
            "USD",
            "JPY",
            "BGN",
            "CZK",
            "DKK",
            "GBP",
            "HUF",
            "PLN",
            "RON",
            "SEK",
            "CHF",
            "ISK",
            "NOK",
            "HRK",
            "RUB",
            "TRY",
            "AUD",
            "BRL",
            "CAD",
            "CNY",
            "HKD",
            "IDR",
            "ILS",
            "INR",
            "KRW",
            "MXN",
            "MYR",
            "NZD",
            "PHP",
            "SGD",
            "THB",
            "ZAR"
        )

        adapterItems =
            ArrayAdapter(this.requireContext(), R.layout.valutes_spinner_list, listOfCurrencies)
        with(binding) {
            with(autoCompleteTextFirstViewCurrency) {
                setAdapter(adapterItems)
                setOnItemClickListener { parent, _, pos, _ ->
                    currencyFrom = parent.getItemAtPosition(pos).toString()
                }
            }

            with(autoCompleteTextViewSecondCurrency) {
                setAdapter(adapterItems)
                setOnItemClickListener { parent, _, pos, _ ->
                    currencyTo = parent.getItemAtPosition(pos).toString()
                }
            }

            buttonConvert.setOnClickListener {
                try {
                    amount = textInputEditTextEnterAmount.text.toString().toDouble()
                    textViewFirstCurrency.text = currencyFrom
                    textViewSecondCurrency.text = currencyTo
                    val key = resources.getString(R.string.api_key)
                    val baseOfCurrency = currencyFrom
                    val listCurrencies = listOf(currencyTo)
                    val descriptionParams = ConvertParams(
                        apiKey = apiKey,
                        currencies = currencies
                    )
                    convertViewModel.getCurrentExchangeRate(key, baseOfCurrency, listCurrencies)
                    convertViewModel.getCurrencyDescription(descriptionParams)
                    binding.textViewSecondCurrencyValue.text =
                        String.format("%.2f", convertResult)
                    binding.textViewFirstCurrencyValue.text = convertResult.toString()

                    observe(convertViewModel.currencyDataSharedFlow) { currencyData ->
                        val currencyFactor = currencyData.currencies[0].currencyValue.replace(",", ".")
                            .toDouble()
                        convertResult = amount * currencyFactor
                        binding.textViewSecondCurrencyValue.text =
                            String.format("%.2f", convertResult)
                        binding.textViewFirstCurrencyValue.text = convertResult.toString()
                    }
                } catch (t: Throwable) {
                    Timber.e("/*/ Error ${t.message}")
                }
            }

            imageViewReverseIcon.setOnClickListener {
                val firstCurrency = textViewFirstCurrency.text.toString()
                val secondCurrency = textViewSecondCurrency.text.toString()

                textViewFirstCurrency.text = firstCurrency
                textViewSecondCurrency.text = secondCurrency
            }

            btnLogout.setOnClickListener {
                replace(LoginFragment())
                Firebase.auth.signOut()
            }
        }
    }

    private fun showData(data: List<Valute>) {
        adapter.setData(data)
    }

    private fun showLoading(isLoading: Boolean) {
        binding.progressBar.isVisible = isLoading
    }
}