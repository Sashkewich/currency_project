package com.example.currency_project.current_exchange_rate.interactor

import com.example.currency_project.current_exchange_rate.model.CurrencyDescription
import com.example.currency_project.current_exchange_rate.repository.ConvertParams
import com.example.currency_project.current_exchange_rate.repository.ConvertRepository
import timber.log.Timber
import javax.inject.Inject

class ConvertInteractor @Inject constructor(
    private val repository: ConvertRepository
) {
    suspend fun getCurrencyDescription(params: ConvertParams): CurrencyDescription {
        Timber.i("/*/ ${repository.getCurrencyDescription(params = params)}")
        return repository.getCurrencyDescription(params = params)
    }
}
