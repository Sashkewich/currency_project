package com.example.currency_project.current_exchange_rate.api.models

import com.google.gson.annotations.SerializedName

data class CurrencyDescriptionResponse(
    @SerializedName("data")
    val currency: CurrencyResponse
)
