package com.example.currency_project.current_exchange_rate.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.currency_project.current_exchange_rate.model.Valute
import com.example.currency_project.databinding.ItemCurrencyBinding

class CurrentExchangeRateViewHolder (
    private val binding: ItemCurrencyBinding,
    private val clickOnItem: (Valute) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    constructor(
        parent: ViewGroup,
        onClickItem: (Valute) -> Unit
    ) : this(
        ItemCurrencyBinding.inflate(LayoutInflater.from(parent.context), parent, false),
        onClickItem
    )

    private lateinit var currencyType: String
    private lateinit var currencyValue: String
    private lateinit var item: Valute

    fun getData(item: Valute) {
        currencyType = item.currencyType
        currencyValue = item.currencyValue
        this.item = item
    }

    fun onBind() {
        with(binding) {
            valuteCode.text = currencyType
            valuteValue.text = currencyValue
        }
    }
}
