package com.example.currency_project.current_exchange_rate.di

import com.example.currency_project.current_exchange_rate.repository.ConvertRemoteRepository
import com.example.currency_project.current_exchange_rate.repository.ConvertRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class ConvertRepositoryModule {

    @Binds
    @Singleton
    abstract fun bindConvertRepository(
        repository: ConvertRemoteRepository
    ): ConvertRepository
}
