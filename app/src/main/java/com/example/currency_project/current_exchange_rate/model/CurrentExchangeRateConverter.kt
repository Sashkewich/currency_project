package com.example.currency_project.current_exchange_rate.model

import com.example.currency_project.current_exchange_rate.api.models.CurrentExchangeRateResponse
import com.example.currency_project.current_exchange_rate.ui.CurrencyType
import com.example.currency_project.utils.extensions.formatDouble

object CurrentExchangeRateConverter {
     fun convert(response: CurrentExchangeRateResponse): CurrentExchangeRate {
        val currencies:List<Valute> by lazy {
            listOf(
                Valute(CurrencyType.AUD.currency, response.data.AUD.formatDouble()),
                Valute(CurrencyType.BGN.currency, response.data.BGN.formatDouble()),
                Valute(CurrencyType.BRL.currency, response.data.BRL.formatDouble()),
                Valute(CurrencyType.CAD.currency, response.data.CAD.formatDouble()),
                Valute(CurrencyType.CHF.currency, response.data.CHF.formatDouble()),
                Valute(CurrencyType.CNY.currency, response.data.CNY.formatDouble()),
                Valute(CurrencyType.CZK.currency, response.data.CZK.formatDouble()),
                Valute(CurrencyType.DKK.currency, response.data.DKK.formatDouble()),
                Valute(CurrencyType.EUR.currency, response.data.EUR.formatDouble()),
                Valute(CurrencyType.GBP.currency, response.data.GBP.formatDouble()),
                Valute(CurrencyType.HKD.currency, response.data.HKD.formatDouble()),
                Valute(CurrencyType.HRK.currency, response.data.HRK.formatDouble()),
                Valute(CurrencyType.HUF.currency, response.data.HUF.formatDouble()),
                Valute(CurrencyType.IDR.currency, response.data.IDR.formatDouble()),
                Valute(CurrencyType.ILS.currency, response.data.ILS.formatDouble()),
                Valute(CurrencyType.INR.currency, response.data.INR.formatDouble()),
                Valute(CurrencyType.ISK.currency, response.data.ISK.formatDouble()),
                Valute(CurrencyType.JPY.currency, response.data.JPY.formatDouble()),
                Valute(CurrencyType.KRW.currency, response.data.KRW.formatDouble()),
                Valute(CurrencyType.MXN.currency, response.data.MXN.formatDouble()),
                Valute(CurrencyType.MYR.currency, response.data.MYR.formatDouble()),
                Valute(CurrencyType.NOK.currency, response.data.NOK.formatDouble()),
                Valute(CurrencyType.NZD.currency, response.data.NZD.formatDouble()),
                Valute(CurrencyType.PHP.currency, response.data.PHP.formatDouble()),
                Valute(CurrencyType.PLN.currency, response.data.PLN.formatDouble()),
                Valute(CurrencyType.RON.currency, response.data.RON.formatDouble()),
                Valute(CurrencyType.RUB.currency, response.data.RUB.formatDouble()),
                Valute(CurrencyType.SEK.currency, response.data.SEK.formatDouble()),
                Valute(CurrencyType.SGD.currency, response.data.SGD.formatDouble()),
                Valute(CurrencyType.THB.currency, response.data.THB.formatDouble()),
                Valute(CurrencyType.TRY.currency, response.data.TRY.formatDouble()),
                Valute(CurrencyType.USD.currency, response.data.USD.formatDouble()),
                Valute(CurrencyType.ZAR.currency, response.data.ZAR.formatDouble())
            )
        }

        return CurrentExchangeRate(
            currencies = currencies
        )
    }
}