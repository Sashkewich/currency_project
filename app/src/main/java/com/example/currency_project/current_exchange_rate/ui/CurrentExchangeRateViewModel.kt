package com.example.currency_project.current_exchange_rate.ui

import com.example.currency_project.common.mvvm.BaseViewModel
import com.example.currency_project.current_exchange_rate.interactor.CurrentExchangeRateInteractor
import com.example.currency_project.current_exchange_rate.model.CurrentExchangeRate
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class CurrentExchangeRateViewModel @Inject constructor(
    private val interactor: CurrentExchangeRateInteractor
) : BaseViewModel() {
    private val _currencyData = MutableSharedFlow<CurrentExchangeRate>().apply {
        @OptIn(ExperimentalCoroutinesApi::class)
        resetReplayCache()
    }

    val currencyData: SharedFlow<CurrentExchangeRate>
        get() = _currencyData.asSharedFlow()

    private val _loadingStateFlow = MutableStateFlow(false)
    val loadingStateFlow: StateFlow<Boolean>
        get() = _loadingStateFlow.asStateFlow()

    fun getCurrentExchangeRate(
        apiKey: String,
        baseCurrency: String,
        currencies: List<String>
    ) {
        launch {
            try {
                _loadingStateFlow.tryEmit(true)
                val currency = interactor.getCurrentExchangeRate(apiKey, baseCurrency, currencies)
                _currencyData.emit(currency)
                _currencyData.combine(_loadingStateFlow) { _, loading ->
                    loading
                }.collect {
                    Timber.d("=== combine $it ===")
                }
            } catch (t: Throwable) {
                Timber.e(t.message)
            } catch (e: CancellationException) {
                Timber.e(e.message)
            } finally {
                _loadingStateFlow.tryEmit(false)
            }
        }
    }
}