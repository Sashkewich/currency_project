package com.example.currency_project.current_exchange_rate.api

import com.example.currency_project.current_exchange_rate.api.models.CurrentExchangeRateResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {
    @GET("v1/latest")
    suspend fun getCurrentExchangeRate(
        @Query("apikey") apiKey: String,
        @Query("base_currency") baseCurrency: String,
        @Query("currencies") currencies: List <String>,
    ): CurrentExchangeRateResponse
}