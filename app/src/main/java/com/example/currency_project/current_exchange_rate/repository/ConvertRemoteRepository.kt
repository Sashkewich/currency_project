package com.example.currency_project.current_exchange_rate.repository

import com.example.currency_project.current_exchange_rate.api.ConvertApi
import com.example.currency_project.current_exchange_rate.model.ConvertConverter
import com.example.currency_project.current_exchange_rate.model.CurrencyDescription
import javax.inject.Inject

class ConvertRemoteRepository @Inject constructor(
    private val api: ConvertApi
) : ConvertRepository {
    override suspend fun getCurrencyDescription(
        params: ConvertParams
    ): CurrencyDescription {
        return ConvertConverter.convert(
            api.getCurrencyDescription(
                apiKey = params.apiKey,
                currencies = params.currencies
            )
        )
    }
}
