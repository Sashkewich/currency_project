package com.example.currency_project.current_exchange_rate.ui

import com.example.currency_project.common.mvvm.BaseViewModel
import com.example.currency_project.current_exchange_rate.interactor.ConvertInteractor
import com.example.currency_project.current_exchange_rate.interactor.CurrentExchangeRateInteractor
import com.example.currency_project.current_exchange_rate.model.CurrencyDescription
import com.example.currency_project.current_exchange_rate.model.CurrentExchangeRate
import com.example.currency_project.current_exchange_rate.repository.ConvertParams
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ConvertViewModel @Inject constructor(
    private val interactorData: CurrentExchangeRateInteractor,
    private val interactorDescription: ConvertInteractor
) : BaseViewModel() {

    private val _currencyDataSharedFlow = MutableSharedFlow<CurrentExchangeRate>().apply {
        @OptIn(ExperimentalCoroutinesApi::class)
        resetReplayCache()
    }
    val currencyDataSharedFlow: SharedFlow<CurrentExchangeRate>
        get() = _currencyDataSharedFlow.asSharedFlow()

    private val _currencyDescriptionSharedFlow = MutableSharedFlow<CurrencyDescription>().apply {
        @OptIn(ExperimentalCoroutinesApi::class)
        resetReplayCache()
    }
    val currencyDescriptionSharedFlow: SharedFlow<CurrencyDescription>
        get() = _currencyDescriptionSharedFlow.asSharedFlow()

    private val _loadingStateFlow = MutableStateFlow(false)
    val loadingStateFlow: StateFlow<Boolean>
        get() = _loadingStateFlow.asStateFlow()

    fun getCurrentExchangeRate(
        apiKey: String,
        baseCurrency: String,
        currencies: List<String>
    ) {
        launch {
            try {
                _loadingStateFlow.tryEmit(true)
                val currencyData = interactorData.getCurrentExchangeRate(apiKey, baseCurrency, currencies)
                _currencyDataSharedFlow.emit(currencyData)
                Timber.i("/*/ Success --->>> $currencyData")
            } catch (t: Throwable) {
                Timber.e("/*/ Error ${t.message}")
            } catch (c: CancellationException) {
                Timber.e("/*/ Error ${c.message}")
            } finally {
                _loadingStateFlow.tryEmit(false)
            }
        }
    }

    fun getCurrencyDescription(params: ConvertParams) {
        launch {
            try {
                _loadingStateFlow.tryEmit(true)
                val currencyData = interactorDescription.getCurrencyDescription(params)
                _currencyDescriptionSharedFlow.emit(currencyData)
                Timber.i("/*/ Success --->>> $currencyData")
            } catch (t: Throwable) {
                Timber.e("/*/ Error ${t.message}")
            } catch (c: CancellationException) {
                Timber.e("/*/ Error ${c.message}")
            } finally {
                _loadingStateFlow.tryEmit(false)
            }
        }
    }
}
