package com.example.currency_project.current_exchange_rate.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CurrencyDescription(
    val symbol: String?,
    val name: String?,
    val symbolNative: String?,
    val decimalDigits: Int?,
    val rounding: Int?,
    val code: String?,
    val namePlural: String?
) : Parcelable


