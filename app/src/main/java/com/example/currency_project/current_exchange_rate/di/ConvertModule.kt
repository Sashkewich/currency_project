package com.example.currency_project.current_exchange_rate.di

import com.example.currency_project.current_exchange_rate.api.ConvertApi
import com.example.currency_project.current_exchange_rate.interactor.ConvertInteractor
import com.example.currency_project.current_exchange_rate.repository.ConvertRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ConvertModule {

    @Provides
    @Singleton
    fun provideConvertApi(retrofit: Retrofit): ConvertApi = retrofit.create(ConvertApi::class.java)

    @Provides
    @Singleton
    fun provideConvertInteractor(repository: ConvertRepository): ConvertInteractor =
        ConvertInteractor(repository)
}
