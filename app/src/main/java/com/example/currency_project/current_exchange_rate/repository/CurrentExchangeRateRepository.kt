package com.example.currency_project.current_exchange_rate.repository

import com.example.currency_project.current_exchange_rate.model.CurrentExchangeRate

interface CurrentExchangeRateRepository {
    suspend fun getCurrentExchangeRate(
        apiKey: String,
        baseCurrency: String,
        currencies: List<String>
    ): CurrentExchangeRate
}