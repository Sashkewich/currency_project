package com.example.currency_project.current_exchange_rate.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Valute(
    val currencyType: String,
    val currencyValue: String
) : Parcelable
