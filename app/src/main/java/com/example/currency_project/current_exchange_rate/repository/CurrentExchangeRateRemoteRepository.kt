package com.example.currency_project.current_exchange_rate.repository

import com.example.currency_project.current_exchange_rate.api.CurrencyApi
import com.example.currency_project.current_exchange_rate.model.CurrentExchangeRate
import com.example.currency_project.current_exchange_rate.model.CurrentExchangeRateConverter
import javax.inject.Inject

class CurrentExchangeRateRemoteRepository @Inject constructor(
    private val api: CurrencyApi
    ) : CurrentExchangeRateRepository {
    override suspend fun getCurrentExchangeRate(
        apiKey: String,
        baseCurrency : String,
        currencies : List<String>
    ) =
        CurrentExchangeRateConverter.convert(
            api.getCurrentExchangeRate(
                apiKey = apiKey,
                baseCurrency = baseCurrency,
                currencies = currencies,
            )
        )

}