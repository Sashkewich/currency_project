package com.example.currency_project.login.ui

import android.content.ContentValues.TAG
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.currency_project.R
import com.example.currency_project.common.mvvm.BaseFragment
import com.example.currency_project.current_exchange_rate.ui.CurrentExchangeRateFragment
import com.example.currency_project.databinding.FragmentLoginBinding
import com.example.currency_project.register.ui.RegisterFragment
import com.example.currency_project.root.RootActivity
import com.example.currency_project.utils.extensions.replace
import com.example.currency_project.utils.extensions.viewbinding.viewBinding
import com.google.firebase.auth.FirebaseAuth
import timber.log.Timber


class LoginFragment : BaseFragment(R.layout.fragment_login) {
    private val binding: FragmentLoginBinding by viewBinding()
    private lateinit var auth: FirebaseAuth

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        auth = FirebaseAuth.getInstance()

        with(binding){
            registerNow.setOnClickListener {
                replace(RegisterFragment())
            }

            btnLogin.setOnClickListener {
                progressBar.visibility = View.VISIBLE
                val email = emailInputText.text.toString()
                val password = passwordInputText.text.toString()

                if (TextUtils.isEmpty(email)){
                    Toast.makeText(context, "Enter email", Toast.LENGTH_SHORT).show()
                }

                if (TextUtils.isEmpty(password)){
                    Toast.makeText(context, "Enter email", Toast.LENGTH_SHORT).show()
                }


                auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(RootActivity()) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Timber.tag(TAG).d("signInWithEmail:success")
                            progressBar.visibility = View.GONE
                            val user = auth.currentUser
                            replace(CurrentExchangeRateFragment())
                        } else {
                            // If sign in fails, display a message to the user.
                            Timber.tag(TAG).w(task.exception, "signInWithEmail:failure")
                            progressBar.visibility = View.GONE
                            Toast.makeText(
                                context,
                                "Authentication failed - invalid email and/or mail entered!",
                                Toast.LENGTH_SHORT,
                            ).show()
                        }
                    }
            }
        }
    }
}