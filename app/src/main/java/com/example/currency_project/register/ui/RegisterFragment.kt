package com.example.currency_project.register.ui

import android.content.ContentValues.TAG
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.example.currency_project.R
import com.example.currency_project.common.mvvm.BaseFragment
import com.example.currency_project.current_exchange_rate.ui.CurrentExchangeRateFragment
import com.example.currency_project.databinding.FragmentRegisterBinding
import com.example.currency_project.login.ui.LoginFragment
import com.example.currency_project.root.RootActivity
import com.example.currency_project.utils.extensions.replace
import com.example.currency_project.utils.extensions.viewbinding.viewBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import timber.log.Timber


class RegisterFragment : BaseFragment(R.layout.fragment_register) {
    private val binding: FragmentRegisterBinding by viewBinding()
    private lateinit var auth: FirebaseAuth


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        auth = Firebase.auth

        with(binding) {
            loginNow.setOnClickListener{
                replace(LoginFragment())
            }

            btnRegister.setOnClickListener {
                progressBar.visibility = View.VISIBLE
                val email = emailInputText.text.toString()
                val password = passwordInputText.text.toString()

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(context, "Enter email", Toast.LENGTH_SHORT).show()
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(context, "Enter password", Toast.LENGTH_SHORT).show()
                }

                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(RootActivity()) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Timber.tag(TAG).d("createUserWithEmail:success")
                            Toast.makeText(
                                context, "Account created successful!",
                                Toast.LENGTH_SHORT
                            ).show()
                            progressBar.visibility = View.GONE
                            val currentUser = auth.currentUser
                            if (currentUser != null) {
                                replace(CurrentExchangeRateFragment())
                            } else {
                                replace(LoginFragment())
                            }

                        } else {
                            // If sign in fails, display a message to the user.
                            Timber.tag(TAG).w(task.exception, "createUserWithEmail:failure")
                            Toast.makeText(
                                context, "Error: Your email does not exist or you entered your email incorrectly!",
                                Toast.LENGTH_SHORT
                            ).show()
                            progressBar.visibility = View.GONE
                        }
                    }
            }
        }
    }
}